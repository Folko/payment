app.controller('areaSchemeCtrl', ['$scope', '$stateParams', 'mainSrv', function($scope, $stateParams, mainSrv) {

	$scope.checkedPlaces = mainSrv.getCheckedPlaces();

	$scope.checkedCount = $scope.checkedPlaces.length;

	$scope.eventID = 0;

	$scope.eventInfo = mainSrv.getEvent("id", 0)[0];

	$scope.eventInfo.areaTitle = mainSrv.getArea($scope.eventInfo.areaID).title;

	$scope.scheme = mainSrv.getEventScheme($scope.eventInfo.schemeID);

	$scope.checkPlace = function(index){
		if ($scope.scheme.places[index].checked == true) {
			$scope.scheme.places[index].checked = false;
			$scope.checkedCount--;

			var checkedID = $scope.scheme.places[index].placeID;

			for(var i=0; i < $scope.checkedPlaces.length; i++) {
				if($scope.checkedPlaces[i].placeID == checkedID) {
					$scope.checkedPlaces.splice(i, 1);
				}
			}
		} else {
			$scope.scheme.places[index].checked = true;
			$scope.checkedCount++;

			$scope.checkedPlaces.push($scope.scheme.places[index]);
		}	
	}

	$scope.buyClick = function(event) {
		if($scope.checkedCount < 1) {
			event.preventDefault();

			return;
		}
	}

	$scope.checkedGetTotal = function() {
		var sum = 0;
		for (var i = 0; i < $scope.checkedPlaces.length; i++) {
			sum = sum + $scope.checkedPlaces[i].cost;
		};
		return sum;
	};

}]);