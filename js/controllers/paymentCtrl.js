app.controller('paymentCtrl', ['$scope', '$stateParams', 'mainSrv', function($scope, $stateParams, mainSrv) {

	$scope.groupItem = mainSrv.getGroupItem($stateParams.groupItemId);

	$scope.fillForm = {};

	$scope.paySum = function() {
		$scope.fillForm.comSum = +($scope.fillForm.paySum) + +($scope.fillForm.paySum/100 * $scope.groupItem.comission);
	};

	$scope.comSum = function() {
		$scope.fillForm.paySum = +($scope.fillForm.comSum) - +($scope.fillForm.comSum/100 * $scope.groupItem.comission);
	};

}]);