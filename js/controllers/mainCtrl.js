app.controller('mainCtrl', ['$scope', '$state', 'mainSrv', function($scope, $state, mainSrv) {

	$scope.currState = $state;

	$scope.currStateName = '';

	$scope.$watch('currState.current.name', function(newValue, oldValue) {
		$scope.currStateName = newValue;
    }); 

	$scope.groups = mainSrv.getGroups();

	$scope.groupItems = mainSrv.getGroupItems();

}]);