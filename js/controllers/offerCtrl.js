app.controller('offerCtrl', ['$scope', '$state', 'mainSrv', function($scope, $state, mainSrv) {

	$scope.eventID = 0;

	$scope.eventInfo = mainSrv.getEvent("id", 0)[0];

	$scope.eventInfo.areaTitle = mainSrv.getArea($scope.eventInfo.areaID).title;

	$scope.offerItems = mainSrv.getCheckedPlaces();

	// если корзина пуста -> переход на выбор мест (защита от опустошения корзины при релоаде страницы)
	if($scope.offerItems.length < 1) {
		$state.go("ticketChoose");
	}


	$scope.offerGetTotal = function() {
		var sum = 0;
		for (var i = 0; i < $scope.offerItems.length; i++) {
			sum = sum + $scope.offerItems[i].cost;
		};
		return sum;
	};
}]);