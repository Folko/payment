app.controller('areaItemCtrl', ['$scope', '$stateParams', 'mainSrv', function($scope, $stateParams, mainSrv) {

	$scope.areaItem = mainSrv.getArea($stateParams.areaId);

	$scope.balloonOpen = function($map){
		var map = $map;
		map.balloon.open($scope.areaItem.coords, $scope.areaItem.title, {
            closeButton: false
        })
    }

}]);