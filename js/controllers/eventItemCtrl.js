app.controller('eventItemCtrl', ['$scope', '$stateParams', 'mainSrv', function($scope, $stateParams, mainSrv) {

	$scope.eventItem = mainSrv.getEvent("id", $stateParams.eventId)[0];

	$scope.area = mainSrv.getArea($scope.eventItem.areaID);

}]);