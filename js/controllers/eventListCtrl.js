app.controller('eventListCtrl', ['$scope', '$state', '$stateParams', 'mainSrv', function($scope, $state, $stateParams, mainSrv) {

	// фильтр по площадкам
	if($stateParams.areaId) {
		$scope.events = mainSrv.getEvent("areaID", $stateParams.areaId);	
	}	

	// фильтр по типам
	if($stateParams.typeId) {
		$scope.events = mainSrv.getEvent("typeID", $stateParams.typeId);	
	}

	// показать все
	if($stateParams.typeId == "all") {
		$scope.events = mainSrv.getEventList();
	}

}]);