app.factory('mainSrv', ['localStorageService', function(localStorageService) {

	var groups = [
		{
			name: "Сотовая связь",
			img: "img/groups/1.png"
		},
		{
			name: "Домофоны",
			img: "img/groups/2.png"
		},
		{
			name: "Коммунальные платежи",
			img: "img/groups/3.png"
		},
		{
			name: "Интернет провайдеры",
			img: "img/groups/4.png"
		},
		{
			name: "Модемный интернет",
			img: "img/groups/5.png"
		}
	];

	var groupItems = [
		{
			name: "Мегафон",
			img: "img/group-items/1.png",
			comission: 10
		},
		{
			name: "МТС",
			img: "img/group-items/2.png",
			comission: null
		},
		{
			name: "ЕТК",
			img: "img/group-items/3.png",
			comission: 146
		},
		{
			name: "Билайн",
			img: "img/group-items/4.png",
			comission: 5
		},
		{
			name: "Теле-2",
			img: "img/group-items/5.png",
			comission: null
		}
	];

	var areas = [
		{
			id: 0,
			title: "Театр Пушкина",
			phone: "+7 (391) 227-35-01",
			address: "г. Красноярск, пр. Мира, 73",
			desc: "Имеется парковка",
			img: "img/area/area1.jpg",
			coords: [92.865579,56.011338] 
		},
		{
			id: 1,
			title: "Гранд Холл Сибирь (МВДЦ 'Сибирь')",
			phone: "+7 (391) 22-88-515",
			address: "г. Красноярск, ул. Авиаторов, 19",
			desc: "Современный концертный и конгрессный центр, по праву считается уникальным для территории Урала, Сибири и Дальнего Востока. Представляет собой здание, соединенное переходом с МВДЦ «Сибирь»; включает парадный зал (1 этаж), большой зал для проведения концертов и конгрессных мероприятий на 1716 мест, 6 конференц-залов, гримерные комнаты, гардероб на 2160 человек.",
			img: "img/area/area2.jpg",
			coords: [92.924733,56.040143]
		}
	];

	var events = [
		{
			id: 0,
			title: "Григорий Лепс. Народный корпоратив",
			date: "16 июля 2015 г.",
			time: "19:30",
			areaID: 0,
			schemeID: 0,
			typeID: 0,
			img: "img/event1.png",
			desc: "19 декабря, в спорткомплексе Олимпийский самый романтичный артист нашей эстрады Стас Михайлов, устраивает грандиозное шоу- народный корпоратив.  Спешите сделать подарок своим родным и близким! Вас ждет чувственная атмосфера, любви и лирики, только лучшие хиты, световое шоу и живой звук! Кульминацией концерта станет розыгрыш автомобиля! Не упустите свой шанс! Увидимся 19 декабря в Олимпийском!"
		},
		{
			id: 4,
			title: "The Prodigy",
			date: "9 октября 2015 г.",
			time: "20:00",
			areaID: 1,
			schemeID: 0,
			typeID: 0,
			img: "img/event2.jpg",
			desc: "Самые бесшабашные воины рейва в полной боеготовности"
		},
		{
			id: 2,
			title: "Linkin Park",
			date: "29 августа 2015 г.",
			time: "19:00",
			areaID: 0,
			schemeID: 0,
			typeID: 1,
			img: "img/event3.png",
			desc: "LINKIN PARK возвращаются в Москву этим летом!"
		}
	];

	var eventSchemes = [
		{
			id: 0,
			areaW: 400,
			areaH: 400,
			places: [
				{		
					placeID: 0,			
					coords: "50,50 70,50 70,70 50,70",
					fill: "#98fb98",
					cost: 1500,
					row: 1,
					section: "Стандарт",
					placeNumber: 1,
					busy: false,
					checked: false
				},
				{		
					placeID: 1,			
					coords: "75,50 95,50 95,70 75,70",					
					fill: "#98fb98",
					cost: 1500,
					row: 1,
					section: "Стандарт",
					placeNumber: 2,
					busy: true,
					checked: false
				},
				{		
					placeID: 2,			
					coords: "100,50 120,50 120,70 100,70",					
					fill: "#98fb98",
					cost: 1500,
					row: 1,
					section: "Стандарт",
					placeNumber: 3,
					busy: false,
					checked: false
				},
				{		
					placeID: 3,			
					coords: "125,50 145,50 145,70 125,70",					
					fill: "#98fb98",
					cost: 1500,
					row: 1,
					section: "Стандарт",
					placeNumber: 4,
					busy: false,
					checked: false
				}
			],
			placeNumbers: [
				{
					coordX: 50,
					coordY: 50,
					text: 1
				},
				{
					coordX: 75,
					coordY: 50,
					text: 2
				},
				{
					coordX: 100,
					coordY: 50,
					text: 3
				},
				{
					coordX: 125,
					coordY: 50,
					text: 4
				}
			],
			labels: [
				{
					coordX: 0,
					coordY: 50,
					text: "Ряд 1"
				},
				{
					coordX: 0,
					coordY: 75,
					text: "Ряд 2"
				},
				{
					coordX: 0,
					coordY: 100,
					text: "Ряд 3"
				}
			]
		}
	];

	var checkedPlaces = [];

	return {
		getGroups: function(){
			return groups;
		},
		getGroupItems: function(){
			return groupItems;
		},
		getGroupItem: function(id){
			return groupItems[id];
		},
		getOfferTemp: function(){
			return offerTemp;
		},
		getEvent: function(key, value){
			var event  = _.filter(events, function(chr) {
				return chr[key] == value;
			});

			return event;
		},
		getEventList: function(){
			return events;
		},
		getAreaList: function(){
			return areas;
		},
		getArea: function(param){
			var area  = _.find(areas, function(chr) {
				return chr.id == param;
			});

			return area;
		},
		getEventScheme: function(id){
			var eventScheme  = _.find(eventSchemes, function(chr) {
				return chr.id == id;
			});

			return eventScheme;
		},
		getCheckedPlaces: function(){
			return checkedPlaces;
		}
	};
}]);