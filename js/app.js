var app = angular.module('app', ['LocalStorageModule', 'ui.router', 'satellizer', 'yaMap']);

app.config(['$urlRouterProvider', '$stateProvider', function($urlRouterProvider, $stateProvider) {
 	$stateProvider

    .state('dash', {
        url: "/dash",
        views: {
            main: {
                templateUrl: 'templates/dash.html'
            },
            sidebar: {
                templateUrl: 'templates/sidebar.html'
            }
        }
    })
    .state('groups', {
        url: "/groups",
        views: {
        	main: {
        		templateUrl: 'templates/groups.html'
        	},
            sidebar: {
                templateUrl: 'templates/sidebar.html'
            }
        }
    })
    .state('groups.items', {
        url: "/group-items/:groupId",
        views: {
        	"main@": {
        		templateUrl: 'templates/group-items.html'
        	}
        }
    })
    .state('payment', {
        url: "/payment",
        views: {
            main: {
                templateUrl: 'templates/payment/payment.html'
            },
            sidebar: {
                templateUrl: 'templates/sidebar.html'
            }
        }
    })
    .state('payment.fill', {
        url: "/fill/:groupItemId",
        views: {
            payment: {       
                controller: 'paymentCtrl',        
                templateUrl: 'templates/payment/fill.html'
            }
        }
    })
    .state('payment.fill.accept', {
        url: "/accept",
        views: {
            "payment@payment": {
                templateUrl: 'templates/payment/accept.html'
            }
        }
    })
    .state('payment.fill.accept.pay', {
        url: "/pay",
        views: {
            "payment@payment": {
                templateUrl: 'templates/payment/pay.html'
            }
        }
    })
    .state('payment.fill.accept.pay.result', {
        url: "/result",
        views: {
            "payment@payment": {
                templateUrl: 'templates/payment/result.html'
            }
        }
    })
    .state('profile', {
        url: "/profile",
        views: {
            main: {
                templateUrl: 'templates/profile/index.html'
            },
            sidebar: {
                templateUrl: 'templates/sidebar.html'
            }
        }
    })
    .state('profile.settings', {
        url: "/settings",
        views: {
            "main@": {
                templateUrl: 'templates/profile/profile-settings.html'
            }
        }
    })
    .state('ticketChoose', {
        url: "/ticket-choose/:schemeID",
        views: {
            main: {
                templateUrl: 'templates/tickets/ticket-choose.html',
                controller: 'areaSchemeCtrl'
            },
            sidebar: {
                templateUrl: 'templates/sidebar.html',
                controller: 'areaSchemeCtrl'
            }
        }
    })
    .state('ticketChoose.ticketOffer', {
        url: "/ticket-offer",
        views: {
            "main@": {
                templateUrl: 'templates/tickets/ticket-offer.html',
                controller: 'offerCtrl'
            }
        }
    })
    .state('eventList', {
        url: "/event-list?:areaId, /event-list?:typeId",
        views: {
            main: {
                templateUrl: 'templates/tickets/event-list.html',
                controller: 'eventListCtrl'
            },
            sidebar: {
                templateUrl: 'templates/sidebar.html'
            }
        }
    })
    .state('eventList.item', {
        url: "/event-item/:eventId",
        views: {
            "main@": {
                templateUrl: 'templates/tickets/event-item.html',
                controller: 'eventItemCtrl'
            }
        }
    })
    .state('areaList', {
        url: "/area-list",
        views: {
            main: {
                templateUrl: 'templates/tickets/area-list.html',
                controller: 'areaListCtrl'
            },
            sidebar: {
                templateUrl: 'templates/sidebar.html'
            }
        }
    })
    .state('areaList.item', {
        url: "/area-item/:areaId",
        views: {
            "main@": {
                templateUrl: 'templates/tickets/area-item.html',
                controller: 'areaItemCtrl'
            }
        }
    })
    

    $urlRouterProvider.otherwise('/groups');
}]);

app.config(['localStorageServiceProvider', function(localStorageServiceProvider) {
    localStorageServiceProvider
        .setPrefix('payment')
        .setStorageType('localStorage')
        .setNotify(true, true)
}]);

app.config(function($authProvider) {
    $authProvider.oauth2({
      name: 'foursquare',
      url: '/auth/foursquare',
      redirectUri: window.location.origin,
      clientId: 'MTCEJ3NGW2PNNB31WOSBFDSAD4MTHYVAZ1UKIULXZ2CVFC2K',
      authorizationEndpoint: 'https://foursquare.com/oauth2/authenticate',
    });
});